#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import json
from gazebo_msgs.srv import GetModelState
from std_srvs.srv import Trigger, TriggerResponse
import time
import math
import os
import requests
import subprocess
import atexit
from newtonrider_arena.srv import RemoveModel
from newtonrider_arena.srv import SpawnModel
from newtonrider_arena.srv import EventNewTarget

# What is the best way to provide Vector operators?
# Also where to place so quadrotor can use it as well
class Vector3D:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    def __add__(self, v):
        return Vector3D(self.x+v.x, self.y+v.y, self.z+v.z)
    def __sub__(self, v):
        return Vector3D(self.x-v.x, self.y-v.y, self.z-v.z)
    def __mul__(self, scalar):
        return Vector3D(self.x * scalar, self.y * scalar, self.z * scalar)
    def dot(self, v):
        return self.x * v.x + self.y * v.y + self.z * v.z
    def length(self):
        return math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)

class GameController:

    def __init__(self, robot_name):

        self.time_for_last_achievement = 70.0 # beam this time for last achievement
        # times and spawn positions
        self.all_spawn_time_indices = [
            10, 100, 130, 180, 190
        ]
        self.all_spawn_positions = [
            Vector3D(-1.0, 3.0, 0),
            Vector3D(3.0, 3.0, 0),
            Vector3D(-6.0, 1.0, 0),
            Vector3D(3.0, 1.0, 0),
            Vector3D(-4.0, -1.0, 0)
        ]
        self.all_blocker_positions = [
            Vector3D(1.0, 3.0, 1.0),
            Vector3D(0.0, 2.5, 1.0),
            Vector3D(2.0, 2.5, 1.0)
        ]

        self.maximum_range_to_plant = 0.1 # have to get this close to plant the tree at target

        self.robot_name = robot_name
        self.robot_status = "Paused"

        self.time_passed = 0.0
        self.start_time = 0.0

        self.spawn_marker_index = 0 # used to make unique spawn names
        self.spawn_tree_index = 0 # used to make unique spawn names
        self.spawn_blocker_index = 0 # used to make unique spawn names
        self.spawn_markers = [] # save a list of markers we spawn
        self.spawn_trees = [] # save a list of trees we spawn
        self.spawn_blockers = [] # save a list of blockers we spawn
        self.spawn_targets = [] # save a list of targets we activated

        self.metrics = {
            'Status' : self.robot_status,
            'Time' : self.time_passed,
            'Score' : 'N/A'
        }

        self.git_dir = '/workspace/src/.git'
        commit_id = ''
        riders_project_id = os.environ.get('RIDERS_PROJECT_ID', '')

        # RIDERS_COMMIT_ID
        git_dir_exists = os.path.isdir(self.git_dir)
        if git_dir_exists:

            popen = subprocess.Popen(['git', 'remote', 'get-url', 'origin'], cwd=self.git_dir, stdout=subprocess.PIPE)
            remote_url, error_remote_url = popen.communicate()

            popen = subprocess.Popen(['git', 'name-rev', '--name-only', 'HEAD'], cwd=self.git_dir, stdout=subprocess.PIPE)
            branch_name, error_branch_name = popen.communicate()

            popen = subprocess.Popen(['git', 'rev-parse', 'HEAD'], cwd=self.git_dir, stdout=subprocess.PIPE)
            commit_id, error_commit_id = popen.communicate()

            if error_remote_url is None and remote_url is not None:
                remote_url = remote_url.rstrip('\n').rsplit('@')
                user_name = remote_url[0].rsplit(':')[1][2:]
                remote_name = remote_url[1]

        self.result_metrics = {
            'Status' : self.robot_status,
            'Time' : self.time_passed,
            'Score' : 'N/A',
            'commit_id': commit_id.rstrip('\n'),
        }

        rospy.init_node("game_controller")

        metric_msg_rate = 10
        rate = rospy.Rate(metric_msg_rate)

        self.metric_pub = rospy.Publisher('simulation_metrics', String, queue_size=10)

        self.init_services()

        self.send_api_check = False

        spawn_counter = 0 # used to track when we spawn some new targets

        # hack remove older models - cleans up from a reset
        for i in range(len(self.all_spawn_time_indices)):
            self.remove_model("pine" + str(i))
            self.remove_model("marker" + str(i))
            self.remove_model("blocker" + str(i)) # assuming we won't have more blockers than markers ... this is a hack but will work fine

        # hack - make sure gazebo has removed before we execute spawn
        # otherwise these can happen out of order
        time.sleep(0.5)

        # add blockers
        for pos in self.all_blocker_positions:
            self.add_blocker(pos)

        achieve_target_first_marker_counter = 0
        achieve_above_first_planting_site_counter = 0
        stable_at_planting_site_counter = 0
        count_plantings = 0
        count_spawns = 0

        # get robot start
        resp = self.get_robot_state("quadrotor", "")
        p = resp.pose.position
        startX = p.x
        startY = p.y

        while ~rospy.is_shutdown():

            spawn_counter += 1
            for i in range(len(self.all_spawn_time_indices)):
                if self.all_spawn_time_indices[i] == spawn_counter:
                    self.add_new_target(self.all_spawn_positions[i])
                    count_spawns += 1

            # check if we are in range
            resp = self.get_robot_state("quadrotor", "")
            p = resp.pose.position
            pos = Vector3D(p.x, p.y, p.z)

            bAtPlantingSite = False
            for i in range(len(self.spawn_targets)):
                delta = pos - self.spawn_targets[i]

                # handle achievement 2 - stable position above first seedling
                if i == 0:
                    distXY = math.sqrt(delta.x * delta.x + delta.y * delta.y)
                    if distXY < self.maximum_range_to_plant:
                        achieve_above_first_planting_site_counter += 1
                    else:
                        achieve_above_first_planting_site_counter = 0
                    if achieve_above_first_planting_site_counter > 10:
                        self.achieve(119) # we are stable above the first planting site

                if delta.length() < self.maximum_range_to_plant:
                    bAtPlantingSite = True

                if bAtPlantingSite and stable_at_planting_site_counter > 10:
                    # handle achievement 3 - plant first seedling
                    if i == 0:
                        self.achieve(120) # we are stable at the first planting site

                    # we did it - spawn the tree
                    self.add_pine_tree(self.spawn_targets[i])
                    self.spawn_targets.pop(i)
                    self.remove_model(self.spawn_markers.pop(i)) # remove from list and remove object from gazebo
                    count_plantings += 1
                    break # there should only be one and the loop is no good now

            if bAtPlantingSite:
                stable_at_planting_site_counter += 1
            else:
                stable_at_planting_site_counter = 0

            x = resp.pose.orientation.x
            y = resp.pose.orientation.y
            z = resp.pose.orientation.z
            w = resp.pose.orientation.w

            # TODO: What library should be used to extract euler yaw from quaternion?
            a = 2.0 * (w * z + x * y)
            b = 1.0 - 2.0 * (y * y + z * z)
            drone_angle = math.atan2(a, b)

            first_marker_angle = math.atan2(self.all_spawn_positions[0].y - startY, self.all_spawn_positions[0].x - startX)
            if drone_angle < first_marker_angle + 0.1 and drone_angle > first_marker_angle - 0.1:
                achieve_target_first_marker_counter += 1
            else:
                achieve_target_first_marker_counter = 0
            if achieve_target_first_marker_counter > 10: # one second of stability is enough
                self.achieve(118)

            if count_plantings == len(self.all_spawn_positions):
                self.achieve(121) # we planted all the sites

            if self.robot_status == "Running" and count_spawns > 0 and count_plantings < len(self.all_spawn_positions):
                self.time_passed = rospy.get_time() - self.start_time

            if count_plantings == len(self.all_spawn_positions) and self.time_passed < self.time_for_last_achievement:
                self.achieve(122)

            '''

            elif self.send_api_check == False:
                #print("Sent to API")
                score = 2000/self.time_passed
                self.metrics['Score'] = score
                self.result_metrics['Status'] =  self.metrics['Status']
                self.result_metrics['Time'] =  self.metrics['Time']
                self.result_metrics['Score'] =  self.metrics['Score']
                self.send_to_api(score, False, **self.result_metrics)
                self.send_api_check = True
            '''

            self.publish_metrics()

            rate.sleep()
            if self.robot_status == "Stop":
                break


    def init_services(self):
        s = rospy.Service('/robot_handler', Trigger, self.handle_robot)

        try:
            rospy.wait_for_service("/gazebo/get_model_state", 1.0)
            self.get_robot_state = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)

            resp = self.get_robot_state("quadrotor", "")
            if not resp.success == True:
                self.robot_status = "no_robot"


        except (rospy.ServiceException, rospy.ROSException), e:
            self.robot_status = "no_robot"
            rospy.logerr("Service call failed: %s" % (e,))

        try:
            rospy.wait_for_service("/remove_model", 5.0)
            self.remove_model = rospy.ServiceProxy('remove_model', RemoveModel)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))


        try:
            rospy.wait_for_service("/spawn_model", 5.0)
            self.spawn_model = rospy.ServiceProxy('spawn_model', SpawnModel)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

        try:
            rospy.wait_for_service("/event_new_target", 5.0)
            self.event_new_target = rospy.ServiceProxy('event_new_target', EventNewTarget)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))


        try:
            rospy.wait_for_service("/event_planted_target", 5.0)
            self.event_planted_target = rospy.ServiceProxy('event_planted_target', EventNewTarget)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

    def add_blocker(self, target):
        # spawn a new blocking column
        spawn_name = "blocker" + str(self.spawn_blocker_index) # make a unique name
        self.spawn_blocker_index += 1 # increment so next will be unique name
        self.spawn_blockers.append(spawn_name) # save a list - we need this so we can clean up for a reset
        self.spawn_model("blocker", spawn_name, target.x, target.y, target.z) # create the model

    def add_pine_tree(self, target):
        # spawn a new pine tree
        spawn_name = "pine" + str(self.spawn_tree_index) # make a unique name
        self.spawn_tree_index += 1 # increment so next will be unique name
        self.spawn_trees.append(spawn_name) # save a list - we need this so we can clean up for a reset
        self.spawn_model("tree_pine", spawn_name, target.x, target.y, target.z) # create the model
        self.event_planted_target(target.x, target.y, target.z)

    def add_marker(self, target):
        # spawn a new marker
        spawn_name = "marker" + str(self.spawn_marker_index) # make a unique name
        self.spawn_marker_index += 1 # increment so next will be unique name
        self.spawn_markers.append(spawn_name) # save a list - we need this so we can clean up for a reset
        self.spawn_model("marker", spawn_name, target.x, target.y, target.z) # create the model

    def add_new_target(self, target):
        self.add_marker(target)
        self.spawn_targets.append(target)
        self.event_new_target(target.x, target.y, target.z) # tell the user code that a new target has been created

    def handle_robot(self,req):

        if self.robot_status == "Paused":
            self.robot_status = "Running"
            self.start_time = rospy.get_time()

            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "Running":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "Stop":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "no_robot":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )


    def publish_metrics(self):
        self.metrics['Status'] = self.robot_status
        #if self.out_of_sequence:
        #    self.metrics['Status'] = "Gates Out of Sequence"

        # round will make it show as an int (39.0000) for example - need decimal precision
        self.metrics['Time'] = self.time_passed # round(self.time_passed, 2)

        self.metric_pub.publish(json.dumps(self.metrics, sort_keys=True))


    def send_to_api(self, score=0.0, disqualified=False,  **kwargs):
        simulation_id = os.environ.get('RIDERS_SIMULATION_ID', None)
        args = score, disqualified
        try:
            if simulation_id:
                self.send_simulation_results(simulation_id, *args, **kwargs)
            else:
                self.send_branch_results(*args, **kwargs)
        except Exception as e:
            rospy.loginfo('Exception occurred while sending metric: %s', e)
        

    def send_simulation_results(self, simulation_id, score, disqualified, **kwargs):
        host = os.environ.get('RIDERS_HOST', None)
        token = os.environ.get('RIDERS_SIMULATION_AUTH_TOKEN', None)
        create_round_url = '%s/api/v1/simulation/%s/rounds/' % (host, simulation_id)
        kwargs['score'] = kwargs.get('score', score)
        kwargs['disqualified'] = kwargs.get('disqualified', disqualified)
        data = {
            'metrics': json.dumps(kwargs)
        }
        # send actual data
        requests.post(create_round_url, {}, data, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })


    def send_branch_results(self, score, disqualified, **kwargs):
        rsl_host = 'http://localhost:8059'
        # metrics endpoint stores data differently compared to simulation endpoint,
        # hence we change how we send it
        info = {
            'score': score,
            'disqualified': disqualified,
        }
        info.update(kwargs)
        url = '%s/events' % rsl_host
        data = {
            'event': 'metrics',
            'info': json.dumps(info)
        }

        requests.post(url, {}, data, headers={
            'Content-Type': 'application/json',
        })


    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })

if __name__ == '__main__':
    # Name of robot
    controller = GameController("Quadrotor")
